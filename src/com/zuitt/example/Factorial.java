package com.zuitt.example;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input a number: ");
        try {
            int result = 1;
            int n = scanner.nextInt();

            if (n < 0) System.out.println("n must not be less than 0");

            else {
                for (int i = 2; i <= n; i++) {
                    result *= i;
                }

                System.out.println("The factorial of " + n + " is " + result);
            }
        }
        catch (Exception error) {
            System.out.println("Invalid input.");
            error.printStackTrace();
        }

    }
}
